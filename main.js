const VueComponent = require("./vue/ribi.vue")
const ReactComponent = require("./react/ribi.jsx")

module.exports = {
    codedWithReact: ReactComponent,
    codedWithVue: VueComponent
}