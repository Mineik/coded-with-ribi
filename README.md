# Coded with ribi
It just displays that your project was coded with ribi

## Usage

React:
```js
import React from "react"
import {CodedWithReact} from "@mineik/coded-with-ribi"

export default function SampleComponent(){
    return(
        <div>
            <!-- ... -->
            <CodedWithReact>with React</CodedWithReact>
        </div>
    )
}
```

Vue:
```js
<template>
    <div>
        <!-- ... -->
        <coded-with-vue>with Vue</coded-with-vue>
    </div>
</template>
<script>
    import {CodedWithVue} from "@mineik/coded-with-ribi"
    export default{
        components: {CodedWithVue} 
    }
</script>
```

## Contribute
pls dont.