import React from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { library } from "@fortawesome/fontawesome-svg-core"
import { faFish, faCode } from "@fortawesome/free-solid-svg-icons"

library.add(faFish)
library.add(faCode)

export default function CodedWithRibi(props) {
    return (
        <div> <FontAwesomeIcon icon="code" /> with <FontAwesomeIcon icon="fish" />·{props.child}</div>
    )
}